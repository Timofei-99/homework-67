import React from 'react';
import './Pin.css';
import {useDispatch, useSelector} from "react-redux";
import Button from "../UI/Button/Button";

const Pin = () => {
        const dispatch = useDispatch();
        let state = useSelector(state => state.pin);
        const show = useSelector(state => state.incorrect);
        const numbers = useSelector(state => state.numbers);

        const change = e => {
            dispatch({type: 'INPUT', payload: e.target.value});
        };

        const click = (e) => {
            dispatch({type: 'ADD', payload: e.currentTarget.textContent});
        };

        const save = () => {
            dispatch({type: 'SAVE'})
        }

        const dlt = () => {
            dispatch({type: 'DELETE'})
        }

        const stars = [];
        for (let i = 0; i < state.length; i++) {
            stars[i] = '*'
        }

        let classes = ['Pin'];
        if  (show) {
            classes.push('Pin')
        } else if (state === 'Correct!') {
            classes.push('Correct')
        } else if (state === 'Incorrect') {
            classes.push('Incorrect')
        }



        return (
            <div className={classes.join(' ')}>
                <div className='Inpt'>
                    <input type="text"
                           onChange={(e) => change(e)}
                           value={show ? stars.join('') : state}
                    />
                </div>
                <div className='Btns'>
                    {numbers.map(p => {
                        if (p.number === 'E') {
                            return <Button
                                key={p.number}
                                value={p.number}
                                add={e => save(e)}
                            />
                        } else if (p.number === '<') {
                            return <Button
                                key={p.number}
                                value={p.number}
                                add={e => dlt(e)}
                            />
                        } else {
                            return <Button
                                key={p.number}
                                value={p.number}
                                add={e => click(e)}
                            />
                        }

                    })}
                </div>
            </div>
        );
    }
;

export default Pin;