import React from 'react';
import './Button.css';

const Button = (props) => {
    return (
        <button
            className='Btn'
            onClick={ props.add}
        >
            {props.value}
        </button>
    );
};

export default Button;