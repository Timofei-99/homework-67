const initialState = {
    pin: '',
    correct: '5555',
    incorrect: false,
    numbers: [
        {number: 'E'},
        {number: 0},
        {number: '<'},
        {number: 1},
        {number: 2},
        {number: 3},
        {number: 4},
        {number: 5},
        {number: 6},
        {number: 7},
        {number: 8},
        {number: 9},
    ],
};


export const reducer = (state = initialState, action) => {
    if (action.type === 'INPUT') {
        return {...state, pin: action.payload}
    }

    if (action.type === 'ADD') {
        if  (state.pin.length < 4) {
            return {...state, pin: state.pin + action.payload, incorrect: true}
        }
    }

    if (action.type === 'SAVE') {
        if (state.pin === state.correct) {
            return {...state, pin: 'Correct!', incorrect: false}
        } else {
            return {...state, pin: 'Incorrect', incorrect: false}
        }
    }

    if (action.type === 'DELETE') {
        return {...state, pin: state.pin.slice(0, -1)}
    }


    return state;
};